# Guilloche patterns


<img src="img/huge.jpeg" alt="pt" width="900" >


Eyal and I have been recreating a Guilloche Pattern machine developed by [James Nolan Gandy](http://www.jamesnolangandy.com/).
Since some months ago Ive been trying to recreate computationally this type of drawings by abstracting the tip of the pen and adding all the parametric functions that each rotation of the machine provides.

Gandy machines are man powered machines with only 1 DOF! And there are so much rotations there. Then, inside 6.849 class I saw Eyal project about a Kempe simulator.. And the solution was clear then, do not simulate the mathematics of the tip, simulate the full machine itself.


<img src="img/gif2.gif" alt="d1" width="950" >

So after some implementations and analysis about how many rotations and how they rotate each of the two main drivers of this weird linckage, we ended up getting something pretty interesting. Later, we are using the Zund with a tiny end effector we made for ball pens.

And here you can see some results. The javascript tool will be published soon and in this repo you can find a script to transofrm the csv file you can export your spline into a curve in rhino.

<img src="img/d9.jpeg" alt="d1" width="800" >

<img src="img/d1.jpeg" alt="d1" width="800" >
<img src="img/d2.jpeg" alt="d1" width="800" >
<img src="img/d3.jpeg" alt="d1" width="800" >
<img src="img/gif1.gif" alt="d1" width="800" >

<img src="img/d4.jpeg" alt="d1" width="800" >
<img src="img/d5.jpeg" alt="d1" width="800" >
<img src="img/d6.jpg" alt="d1" width="800" >
<img src="img/d7.jpg" alt="d1" width="800" >
<img src="img/d1.jpeg" alt="d1" width="800" >
